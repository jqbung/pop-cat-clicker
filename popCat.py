import json
from time import sleep

import selenium.webdriver as webdriver
import selenium.webdriver.support.ui as ui
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options

HERE_API_URL = 'https://developer.here.com'
HERE_API_SIGNIN_EMAIL = 'sign-in-email',
HERE_API_SIGNIN_PASS = 'sign-in-password-encrypted'
HERE_API_SIGNIN_SUBMIT = 'signInBtn'

driver_options = Options()
driver_options.headless = False

driver = webdriver.Chrome(options=driver_options)

POP_CAT_URL = 'https://popcat.click/'

def popCatSpam(clickTime):
  try:
    driver.get(POP_CAT_URL)
    sleep(5)

    i = 0;
    for i in range(0, clickTime):
      driver.find_element_by_id('app').click()
      sleep(0.1)

    # driver.quit()
  
  except:
    print('Error in open pop cat')
    driver.quit()



try:
  popCatSpam(10000)
except TimeoutException as ex:
  print('Selenium Timeout')
finally:
  driver.quit()
